﻿using System.Collections.Generic;

namespace LatexDocument
{
    public class PreTexto
    {
        /// <summary>
        /// Text to be displayed in the title page
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Date to be displayed in the title page
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// City to be displayed in the title page
        /// </summary>
        public string City { get; set; }
        
        /// <summary>
        /// Autores to be displayed in the cover
        /// </summary>
        public List<string> Autores { get; set; }

        /// <summary>
        /// Instituto to be displayed in the cover
        /// </summary>
        public string Instituto { get; set; }

        /// <summary>
        /// Curso to be displayed in the cover
        /// </summary>
        public string Curso { get; set; }

        public string Orientador { get; set; }

        public string Texto { get; set; }

        public string Acknowledgement { get; set; }
        
        public string Resumo { get; set; }

        public string Abstract { get; set; }
        /// <summary>
        /// Instantiates a new LatexPageTitle object
        /// </summary>
        /// <param name="Title">Text to be displayed in the title page</param>


        public PreTexto(string Instituto, string Curso, List<string> Autores, string Orientador,string Title, string Texto, string City, string Date)
        {
            this.Instituto = Instituto;
            this.Curso = Curso;
            this.Autores = Autores;
            this.Orientador = Orientador;
            this.Title = Title;
            this.Texto = Texto;
            this.City = City;
            this.Date = Date;
        }

        public PreTexto(string Instituto, string Curso, List<string> Autores, string Orientador, string Title, string Texto, string City, string Date, string Acknowledgement)
        {
            this.Instituto = Instituto;
            this.Curso = Curso;
            this.Autores = Autores;
            this.Orientador = Orientador;
            this.Title = Title;
            this.Texto = Texto;
            this.City = City;
            this.Date = Date;
            this.Acknowledgement = Acknowledgement;
        }

        public PreTexto(string Instituto, string Curso, List<string> Autores, string Orientador, string Title, string Texto, string City, string Date, string Acknowledgement,
            string Resumo, string Abstract)
        {
            this.Instituto = Instituto;
            this.Curso = Curso;
            this.Autores = Autores;
            this.Orientador = Orientador;
            this.Title = Title;
            this.Texto = Texto;
            this.City = City;
            this.Date = Date;
            this.Acknowledgement = Acknowledgement;
            this.Resumo = Resumo;
            this.Abstract = Abstract;
        }
    }
}
